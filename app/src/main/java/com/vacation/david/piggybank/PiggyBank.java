package com.vacation.david.piggybank;

/**
 * Created by david on 11/04/17.
 */

public class PiggyBank {

    private static float total;
    private static float owed;

    public PiggyBank(){
        /* Default constructor */
    }

    public void setTotal(float total){
        this.total = total;
    }

    public Float getTotal(){
        return total;
    }
    public void setOwed(float owed){
        this.owed = owed;
    }

    public Float getOwed(){
        return owed;
    }

}
