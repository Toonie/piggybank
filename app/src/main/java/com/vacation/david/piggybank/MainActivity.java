package com.vacation.david.piggybank;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class MainActivity extends AppCompatActivity {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    PiggyBank piggy = new PiggyBank();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final TextView total = (TextView) findViewById(R.id.mTotal);
        final TextView owed = (TextView) findViewById(R.id.mOwed);
        setSupportActionBar(toolbar);

        final DatabaseReference refTotal = database.getReference("total");
        final DatabaseReference refOwed = database.getReference("owed");
        refTotal.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String totalVal = dataSnapshot.getValue().toString();
                total.setText(String.format("$%.2f", Float.parseFloat(totalVal)));
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        refOwed.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String owedVal = dataSnapshot.getValue().toString();
                owed.setText(String.format("$%.2f", Float.parseFloat(owedVal)));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Context context = getApplicationContext();
        Intent intent = new Intent(this, HistoryActivity.class);
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast test = Toast.makeText(context, "History",Toast.LENGTH_LONG);
            test.show();
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }


}
